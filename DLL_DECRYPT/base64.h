#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * base64_encode(char * bindata, char * base64, int binlength );
int base64_decode(char * base64, char * bindata );