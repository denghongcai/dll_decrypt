// DLL_DECRYPT.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include "rc4.h"
#include "windows.h"
#include <iostream>
#include "base64.h"
#define BUF_SIZE 1024

using namespace std;

char * readBinaryRC(char * path, unsigned int *length)
{
	char key[20] = "60vpnadmin123";
	FILE * pfile;
	char * data;
	char * returndata;
	unsigned int filesize;

	pfile = fopen(path, "rb");
	if (pfile == NULL)
	{
		return NULL;
	}
	fseek(pfile, 0, SEEK_END);
	*length = ftell(pfile);
	filesize = *length;
	data = (char *)malloc((*length + 1) * sizeof(char));
	returndata = data;
	rewind(pfile);
	*length = fread(data, 1, *length, pfile);
	rc4 rc4_cona((unsigned char *)key,strlen(key));
	char *buf[BUF_SIZE];
	if( filesize < BUF_SIZE)
	{
		memcpy(buf, data, filesize);
		rc4_cona.rc4_encode((unsigned char*)buf, filesize);
		//注意，每进行一次加密,key就会变换,紧接着使用rc4_encode解密就不正确
		memcpy(data, buf, filesize);
	}
	else
	{
		unsigned int i = 1;
		memcpy(buf, data, BUF_SIZE);
		while (true)
		{
			rc4_cona.rc4_encode((unsigned char*)buf, BUF_SIZE);
			//注意，每进行一次加密,key就会变换,紧接着使用rc4_encode解密就不正确
			memcpy(data, buf, BUF_SIZE);
			//uLen = fread(buf, sizeof(char), BUF_SIZE, fSou);
			i++;
			if( i * BUF_SIZE < filesize)
				data += BUF_SIZE;
			else
			{
				filesize = (filesize - (i-1)*BUF_SIZE);
				memcpy(buf, data + BUF_SIZE, filesize);
				rc4_cona.rc4_encode((unsigned char*)buf, filesize);
				memset(data + BUF_SIZE, 0, filesize);
				memcpy(data + BUF_SIZE, buf, filesize);
				break;
			}
			memset(buf, 0, BUF_SIZE);
			memcpy(buf, data, BUF_SIZE);
		}
	}
	returndata[*length] = '\0';
	fclose(pfile);
	return returndata;
}

char * readBinary(char * path, unsigned int *length)
{
	FILE * pfile;
	char * data;

	pfile = fopen(path, "rb");
	if (pfile == NULL)
	{
		return NULL;
	}
	fseek(pfile, 0, SEEK_END);
	*length = ftell(pfile);
	data = (char *)malloc((*length + 1) * sizeof(char));
	rewind(pfile);
	*length = fread(data, 1, *length, pfile);
	data[*length] = '\0';
	fclose(pfile);
	return data;
}

extern "C" __declspec(dllexport) void decrypt(char* input)
{
	unsigned int filesize;
	char * fp;
	char * basebuffer;
	FILE* fDes;
	char inputtmp[1024];
	strcpy(inputtmp, input);
	strcat(inputtmp, "rc4");
	//fSou = fopen("d:\\rc4.png", "rb");
	/*
	fp = readBinaryRC("d:\\1.png", &filesize);
	basebuffer = new char[(filesize/3+1)*4];
	base64_encode(fp, basebuffer, filesize);
	fDes = fopen("d:\\2.png" , "w+b");
	fwrite(basebuffer, 1, strlen(basebuffer), fDes);
	fclose(fDes);
	*/
	fp = readBinary(input, &filesize);
	basebuffer = new char[filesize];
	filesize = base64_decode(fp, basebuffer);
	fDes = fopen(inputtmp , "w+b");
	fwrite(basebuffer, 1, filesize, fDes);
	fclose(fDes);
	
	fp = readBinaryRC(inputtmp, &filesize);
	fDes = fopen(input , "wb");
	fwrite(fp, 1, filesize, fDes);
	fclose(fDes);
}

extern "C" __declspec(dllexport) char* decryptString(char* input)
{
	unsigned int filesize;
	char * basebuffer;
	FILE* fDes;
	char* fp;
	//char inputtmp[1024];
	//strcpy(inputtmp, input);
	//strcat(inputtmp, "rc4");
	//fSou = fopen("d:\\rc4.png", "rb");
	/*
	fp = readBinaryRC("d:\\1.png", &filesize);
	basebuffer = new char[(filesize/3+1)*4];
	base64_encode(fp, basebuffer, filesize);
	fDes = fopen("d:\\2.png" , "w+b");
	fwrite(basebuffer, 1, strlen(basebuffer), fDes);
	fclose(fDes);
	*/
	basebuffer = new char[strlen(input)];
	filesize = base64_decode(input, basebuffer);
	fDes = fopen("tmp.tmp" , "w+b");
	fwrite(basebuffer, 1, filesize, fDes);
	fclose(fDes);
	
	fp = readBinaryRC("tmp.tmp", &filesize);
	return fp;
}
